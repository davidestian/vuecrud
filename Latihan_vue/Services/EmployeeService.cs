﻿using Latihan_vue.Models;
using LatihanVue.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Latihan_vue.Services
{
    public class EmployeeService
    {
        private readonly TrainingDbContext _Db;

        public EmployeeService(TrainingDbContext db)
        {
            this._Db = db;
        }

        public async Task<List<EmployeeModel>> GetAllEmployeeAsync()
        {
            var employees = await this._Db.Employee.Select(Q => new EmployeeModel
            {
                EmployeeEmail = Q.EmployeeEmail,
                EmployeeId = Q.EmployeeId,
                EmployeeName = Q.EmployeeName,
                EmployeePassword = Q.EmployeePassword
            }).ToListAsync();
            return employees;
        }
        public async Task<EmployeeModel> GetEmployeeById(int id)
        {
            var employee = await this._Db.Employee.Where(Q => Q.EmployeeId == id).FirstOrDefaultAsync();
            var data = new EmployeeModel
            {
                EmployeeId = employee.EmployeeId,
                EmployeeEmail = employee.EmployeeEmail,
                EmployeeName = employee.EmployeeName,
                EmployeePassword = employee.EmployeePassword
            };
            return data;
        }
        public async Task<bool> InsertEmployeeAsync(EmployeeModel employee)
        {
            var insertData = new Employee
            {
                EmployeeEmail = employee.EmployeeEmail,
                EmployeeName = employee.EmployeeName,
                EmployeePassword = employee.EmployeePassword
            };
            this._Db.Employee.Add(insertData);
            await this._Db.SaveChangesAsync();
            return true;
        }
        public async Task<bool> EditEmployeeAsync(EmployeeModel employee)
        {
            var targetEmployee = await this._Db.Employee.Where(Q => Q.EmployeeId == employee.EmployeeId).FirstOrDefaultAsync();
            targetEmployee.EmployeeName = employee.EmployeeName;
            targetEmployee.EmployeePassword = employee.EmployeePassword;
            targetEmployee.EmployeeEmail = employee.EmployeeEmail;
            this._Db.Employee.Update(targetEmployee);
            await this._Db.SaveChangesAsync();
            return true;
        }
        public async Task<bool> DeleteEmployeeAsync(int id)
        {
            var targetEmployee = await this._Db.Employee.Where(Q => Q.EmployeeId == id).FirstOrDefaultAsync();
            this._Db.Employee.Remove(targetEmployee);
            await this._Db.SaveChangesAsync();
            return true;
        }
    }
}
