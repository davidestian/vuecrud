﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Latihan_vue.Models;
using Latihan_vue.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Latihan_vue.API
{
    [Route("api/v1/Employee")]
    [ApiController]
    public class EmployeeApiController : ControllerBase
    {
        private readonly EmployeeService _EmployeeServiceMan;

        public EmployeeApiController(EmployeeService employeeService)
        {
            this._EmployeeServiceMan = employeeService;
        }
        
        [HttpGet("GetAllEmployee")]
        public async Task<IActionResult> GetllAllEmployeeAsync()
        {
            var employees = await _EmployeeServiceMan.GetAllEmployeeAsync();
            
            return Ok(employees);
        }

        [HttpGet("GetEmployee/{id}")]
        public async Task<IActionResult> GetEmployeeAsync(int id)
        {
            var employee = await _EmployeeServiceMan.GetEmployeeById(id);
            if(employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }
        [HttpPost("Edit")]
        public async Task<IActionResult> EditEmployeeAsync([FromBody] EmployeeModel employee)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model is not valid");
            }
            var isSuccess = await this._EmployeeServiceMan.EditEmployeeAsync(employee);
            if (isSuccess == false)
            {
                return BadRequest("Edit Employee Failed");
            }
            return Ok("Edit Success");
        }
        [HttpPost("Insert")]
        public async Task<IActionResult> InsertEmployeeAsync([FromBody]EmployeeModel employee)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Model is not valid");
            }
            var isSuccess = await this._EmployeeServiceMan.InsertEmployeeAsync(employee);
            if (isSuccess == false)
            {
                return BadRequest("Insert Employee Failed");
            }
            return Ok("Insert Employee Success");
        }
        [HttpPost("Delete/{id}")]
        public async Task<IActionResult> DeleteEmployeeAsync(int id)
        {
            var employee = await _EmployeeServiceMan.GetEmployeeById(id);
            if (employee == null)
            {
                return NotFound();
            }
            await this._EmployeeServiceMan.DeleteEmployeeAsync(id);
            return Ok("Delete Employee Success");
        }
    }
}
