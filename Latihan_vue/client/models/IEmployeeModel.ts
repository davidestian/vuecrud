﻿export interface IEmployeeModel
{
    employeeId?: number;
    employeeName?: string;
    employeeEmail?: string;
    employeePassword?: string;
}