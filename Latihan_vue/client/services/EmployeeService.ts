﻿import Axios from 'axios';
import { IEmployeeModel } from '../models/IEmployeeModel';

export class EmployeeService
{
    employees: IEmployeeModel[] = [];
    employee: IEmployeeModel = {};
    async insertEmployee(value: IEmployeeModel) {
        try {
            let response = await Axios.post<boolean>('/api/v1/Employee/Insert', value);
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data);
        }
    }
    async getAllEmployee() {
        let response = await Axios.get<IEmployeeModel[]>('/api/v1/Employee/GetAllEmployee');
        this.employees = response.data;
    }
    async getEmployee(value: number) {
        try {
            let response = await Axios.get<IEmployeeModel>('/api/v1/Employee/GetEmployee/' + value);
            console.log(response.data);
            this.employee = response.data;

        } catch (error) {
            console.log(error.response.data);
        }

    }
    async editEmployee(value: IEmployeeModel) {
        try {
            let response = await Axios.post<boolean>('/api/v1/Employee/Edit', value);
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data);
        }
    }
    async deleteEmployee(value: number) {
        try {
            let response = await Axios.post<boolean>('/api/v1/Employee/Delete/' + value);
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data);
        }
    }
}

export let ServiceSingelton = new EmployeeService();