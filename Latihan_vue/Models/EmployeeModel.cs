﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Latihan_vue.Models
{
    public class EmployeeModel
    {
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeeName { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeeEmail { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeePassword { get; set; }
    }
}
