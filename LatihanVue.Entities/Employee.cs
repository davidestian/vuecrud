﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LatihanVue.Entities
{
    public partial class Employee
    {
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeeName { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeeEmail { get; set; }
        [Required]
        [StringLength(255)]
        public string EmployeePassword { get; set; }
    }
}
